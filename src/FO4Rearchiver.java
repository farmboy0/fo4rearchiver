import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FO4Rearchiver {
	private static final String BASE_DIR = "/mnt/work/Vortex/fo4vr";
	private static final String SUB_ORIG = "original";
	private static final String SUB_EXT = "extracted";
	private static final String SUB_SRC = "source";

	private static final String WINE_PRF = "/mnt/daten/Wine/mods";

	private static final String ARCHIVER = "drive_c/GECK/Tools/Archive2/Archive2.exe";

	private static final Set<String> USED_FILES = new ConcurrentSkipListSet<String>();

	private static void removeExtracted(Path extracted) throws IOException {
		Files.walkFileTree(extracted, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	private static int extract(Path extractTo, File archive) throws IOException, InterruptedException {
		String[] cmd = new String[] { //
				new File(WINE_PRF, ARCHIVER).getAbsolutePath(), //
				String.format("Z:%s", archive.getAbsolutePath()), //
				String.format("-e=Z:%s", extractTo.toFile().getAbsolutePath()), //
		};
		File dir = new File(BASE_DIR);

		final ProcessBuilder pb = new ProcessBuilder(cmd).directory(dir).inheritIO();
		pb.environment().put("WINEPREFIX", WINE_PRF);
		pb.environment().put("WINEDEBUG", "-all");
		pb.environment().put("DXVK_LOG_LEVEL", "none");

		final Process p = pb.start();
		return p.waitFor();
	}

	private static int repack(Path extractTo, String archiveName) throws IOException, InterruptedException {
		File base = new File(BASE_DIR);
		String[] cmd = new String[] { //
				new File(WINE_PRF, ARCHIVER).getAbsolutePath(), //
				String.format("Z:%s", findRepackPath(extractTo).toString()), //
				"-f=DDS", // TODO Meshes
				String.format("-c=Z:%s", new File(base, archiveName).getAbsolutePath()), //
				String.format("-r=Z:%s", extractTo.toAbsolutePath().toString()), //
		};

		final ProcessBuilder pb = new ProcessBuilder(cmd).directory(base).inheritIO();
		pb.environment().put("WINEPREFIX", WINE_PRF);
		pb.environment().put("WINEDEBUG", "-all");
		pb.environment().put("DXVK_LOG_LEVEL", "none");

		final Process p = pb.start();
		return p.waitFor();
	}

	private static Path findRepackPath(Path extractTo) {
		final Path textures = extractTo.resolve("Textures");
		final Path meshes = extractTo.resolve("Meshes");
		return textures.toFile().exists() ? textures.toAbsolutePath() : meshes.toAbsolutePath();
	}

	private static Map<String, Path> createMapping(Path parent) throws IOException {
		final Map<String, Path> result = new HashMap<>();
		Files.walkFileTree(parent, new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				final Path rel = parent.relativize(file);
				result.put(rel.toString().toLowerCase(), rel);
				return FileVisitResult.CONTINUE;
			}
		});
		return result;
	}

	private static List<String> findDuplicates(Collection<String> one, Collection<String> two) {
		return one.stream().filter(two::contains).collect(Collectors.toList());
	}

	private static List<String> findMissing(Collection<String> all, Collection<String> found) {
		Predicate<String> p = found::contains;
		return all.stream().filter(p.negate()).collect(Collectors.toList());
	}

	private static void replaceFiles(Path source, Map<String, Path> sourceMap, Path extractTo,
			Map<String, Path> extractMap, List<String> fileKeys) throws IOException {
		for (String key : fileKeys) {
			Files.copy( //
					source.resolve(sourceMap.get(key)), //
					extractTo.resolve(extractMap.get(key)), //
					StandardCopyOption.REPLACE_EXISTING);
		}
	}

	public static void main(String[] args) {
		final File base = new File(BASE_DIR);
		final Path source = new File(base, SUB_SRC).toPath();
		try {
			Map<String, Path> sourceMap = createMapping(source);
			Stream.of(new File(base, SUB_ORIG).listFiles()).forEach(archive -> {
				try {
					Path extractTo = Files.createTempDirectory(base.toPath(), SUB_EXT);
					extract(extractTo, archive);
					Map<String, Path> extractMap = createMapping(extractTo);
					List<String> duplicates = findDuplicates(sourceMap.keySet(), extractMap.keySet());
					if (!duplicates.isEmpty()) {
						replaceFiles(source, sourceMap, extractTo, extractMap, duplicates);
						repack(extractTo, archive.getName());
						USED_FILES.addAll(duplicates);
					}
					removeExtracted(extractTo);
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			});
			List<String> missing = findMissing(sourceMap.keySet(), USED_FILES);
			System.out.println("The following files could not be packed:");
			missing.stream().sorted().forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}
}
